# The XFone Installer config is a fork of the UBports Installer configs [Source](https://github.com/ubports/installer-configs/)

This repository contains the config files for [Xfone Installer](https://gitlab.com/rubencarneiro/xfone-installer) devices.
## Contributing

If you want to add a or improve a device, run `npm run validate` to make sure your file follows the specification and `npm run lint` to make it pretty. You can use `npm run checkdownloads` to make sure all files download successfully.

You can use your local config file with the [XFone Installer](https://gitlab.com/rubencarneiro/xfone-installer) by supplying the `--file` or `-f` flag:

```
xfone-installer -f ./path/to/config.yml
```
#### Migrate

You can use the migration script to quickly migrate a `v1` config to `v2`:

```
$ ./v2/migrate.js -h
Usage: ./v2/migrate.js -i ./path/to.json

migrate a v1 JSON config to v2 YAML

Options:
  -V, --version               output the version number
  -i, --input <path to json>  v1 file (default: undefined)
  -h, --help                  output usage information
```
The specification includes two general files, that are written manually: The `index.json` file lists all available devices. If the installer detects a device automatically, it can query `aliases.json` to resolve the detected string to the canonical codename for the device.

Every device is configured by a `<codename>.json` file that contains information about the device, as well as a list of steps to install different operating systems. Re-usable instructions for the user ("user actions") can also be specified, as well as configuration options for each available operating system.

## License

Original development by [Jan Sprinz](https://spri.nz). Copyright (C) 2019-2020 [UBports Foundation](https://ubports.com).

Modify by [Rúben Carneiro.](https://rubencarneiro.github.io/rubencarneiro.io/) Copyright 2021 [XFone Mobile](https://xfonemobile.com/).

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
